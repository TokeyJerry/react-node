
- docs 目录
    + [一、React 浅析](./一、React 浅析.md)
    + [二、DOM DIFF 算法](./二、DOM DIFF 算法.md)
    + [三、React 组件的生命周期](三、React 组件的生命周期.md)
    + 四、React 组件间的数据传递
    + [五、虚拟 DOM 及内核 Virtual DOM and Internals](五、虚拟 DOM 及内核 Virtual DOM and Internals.md)

## React 组件间的数据传递

> React 的最大好处在于：功能组件化，遵守前端可维护的原则。而组建之间的通信、数据传递如何实现，正是本文所探讨的内容。

> React 组件间数据传输方式：单项数据流。React 是单向数据流，数据主要从父节点传递到子节点（通过 `props`）。
>
> 如果顶层（父级）的某个 `props` 改变了，React 会重渲染所有的子节点。

#### 1、props

#### 2、状态提升

#### 3、context

- childContextTypes 【数据定义类型校验】(顶级组件)

    还有一个看起来很可怕的 childContextTypes，它的作用其实 propsType 验证组件 props 参数的作用类似。不过它是验证 getChildContext 返回的对象。为什么要验证 context，因为 context 是一个危险的特性，按照 React.js 团队的想法就是，把危险的事情搞复杂一些，提高使用门槛人们就不会去用了。如果你要给组件设置 context，那么 childContextTypes 是必写的。

    ```js
    static childContextTypes = {
      <key>: PropTypes.<type>[, <key>: PropTypes.<type>, ···]
    }
    ```

    <type> 所校验的类型请参考：(prop-types)[https://github.com/facebook/prop-types]

- getChildContext 【数据定义】(顶级组件)

    getChildContext 这个方法就是设置 context 的过程，它返回的对象就是 context，所有的子组件都可以访问到这个对象。

    ```js
    getChildContext () {
      return { 
        <key>: <value>[, <key>: <value>, ···]
      }
    }
    ```

- contextTypes 【数据获取类型校验】(子组件)

    子组件要获取 context 里面的内容的话，就必须写 contextTypes 来声明和验证你需要获取的状态的类型，它也是必写的，如果你不写就无法获取 context 里面的状态。

    ```js
    static contextTypes = {
      <key>: PropTypes.<type>[, <key>: PropTypes.<type>, ···]
    }
    ```

- 子组件获取数据

    ```js
    this.context.<key>
    ```

#### 4、redux

#### 5、回调渲染模式(Render Callback Pattern)

> 回调渲染模式(Render Callback Pattern)中，组件会接收某个函数作为子组件，然后在渲染函数中以 `props.children` 进行调用。

- 父组件

    ```js
    class Twitter extends Component {

      constructor () {
        super();
        this.state = {
          user: {
            username: 'alalei'
          }
        }
      }

      render () {
        return this.props.children(this.state.user)
      }
    }

    ReactDOM.render(
      <Twitter username='chuhan'>
        {(user)=> user === null ? <Loading /> : <Badage info = {user}/>}
      </Twitter>,
      document.getElementById('root')
    )
    ```

    此模式的优势在于将父组件与子组件解耦,子组件可以直接访问父组件的内部状态而不需要再通过 Props 传递,这样父组件能够更为方便地控制子组件展示的 UI 界面.如果将原本展示的 Badge 替换为 Profile ,可以方便的修改回调函数来实现。

### 参考资料

- https://blog.csdn.net/guangyao88/article/details/71713806
- https://www.cnblogs.com/tim100/p/6050514.html
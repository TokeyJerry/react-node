
## React 开发规范

### 一、组件的命名

- 1.组件的私有方法都用 `_` 开头（这里尽量不要有对页面重新渲染的动作 [setState]，方便文件拆分）
    
    ```js
    _loadUserName () {
      const userName = this.props.userName;
      if (userName) {
        // do something
      }
    }
    ```

- 2.所有事件监听的方法都用 `handle` 开头

    ```js
    <div
      onClick={this.handleClick.bind(this)} >
      click me
    </div>·
    ```

- 3.把事件监听方法传给组件的时候，属性名用 `on` 开头

    ```js
    <CommentInput
      onSubmit={this.handleSubmitComment.bind(this)} />
    ```

### 二、组件的内容编写顺序

- 1.static 开头的静态类属性，如 `defaultProps`、`propTypes`。
- 2.构造函数，`constructor`。
- 3.`getter/setter`。
- 4.组件生命周期。
- 5.`_` 开头的私有方法。
- 6.事件监听方法，`handle*`。
- 7.`render*` 开头的方法，有时候 `render()` 方法里面的内容会分开到不同函数里面进行，这些函数都以 `render*` 开头。
- 8.`render()` 方法。

### 三、reducer 文件的定义顺序

- 1.定义 action types。
- 2.编写 reducer 。
- 3.编写与该 reducer 相关的 action creators。

### 四、组件的存放目录

> 只会接受 props 并且渲染确定结果的组件我们把它叫做 Dumb 组件，这种组件只关心一件事情 —— 根据 props 进行渲染。
> 
> smart 组件是专门做数据相关的应用逻辑，和各种数据打交道、和 Ajax 打交道，然后把数据通过 props 传递给 Dumb，它们带领着 Dumb 组件完成了复杂的应用程序逻辑

- 1.所有的 Dumb 组件都放在 components/ 目录下。
- 2.所有的 Smart 的组件都放在 containers/ 目录下。

### 五、数据的存放规范

- 1.store 存放全局共享数据、或者是某些模块的共享数据。
- 2.一个页面或者一个功能模块，尽量按照关联的数据将数据提升到父组件（利用 props 来获取数据）。
- 3.与组件渲染有关的数据可以存放到 state 中，其他数据可以放到全局变量中（即：组件的私有属性 [this.data]）。

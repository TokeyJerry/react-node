import React, { Component } from 'react';

export default (Main, name) => {
  class LoadData extends Component {
    constructor () {
      super();
      this.state = {
        data: null
      }
    }

    componentDidMount () {
      let data = name;
      this.setState({ data });
    }

    render () {
      return <Main data={this.state.data} />
    }
  }

  return LoadData;
}

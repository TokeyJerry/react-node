
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

// 回调渲染模式(Render Callback Pattern)

class Twitter extends Component {

  constructor () {
    super();
    this.state = {
      user: {
        username: 'alalei'
      }
    }
  }

  render () {
    console.log(this.props.children);
    
    return this.props.children(this.state.user)
  }
}

class Loading extends Component {
  
  render () {
    return (
      <div>
        <span>Loading...</span>
      </div>
    )
  }
}

class Badage extends Component {

  constructor (props) {
    super(props);
    console.log(this.props.info)
  }

  render () {
    return (
      <div>
        <span>My name is {this.props.info.username}</span>
      </div>
    )
  }
}


ReactDOM.render(
  <Twitter username='chuhan'>
    {(user)=> user === null ? <Loading /> : <Badage info = {user}/>}
  </Twitter>,
  document.getElementById('root')
)


import React, { Component } from 'react';
import LinkedStateMixin from 'react-addons-linked-state-mixin';

export default class UseLinkedStateMixin extends Component {

  constructor () {
    super();
    this.state = {
      name: '铁建文'
    };
  }

  render () {
    return (
      <div>
        <h2>二、UseLinkedStateMixin</h2>
        <p><var>我的名字是：</var><span>{this.state.name}</span></p>
        <input type="text" defaultValue={this.state.name} />
      </div>
    )
  }
}

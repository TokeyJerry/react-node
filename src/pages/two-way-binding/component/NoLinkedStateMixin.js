
import React, { Component } from 'react';

export default class NoLinkedStateMixin extends Component {

  constructor () {
    super();
    this.state = {
      name: '铁建文'
    }
  }

  handleChange (e) {
    this.setState({
      name: e.target.value
    })
  }

  render () {
    return (
      <div>
        <h2>一、NoLinkedStateMixin</h2>
        <p><var>我的名字是：</var><span>{this.state.name}</span></p>
        <input type="text" onChange={this.handleChange.bind(this)} value={this.state.name} />
      </div>
    )
  }
}

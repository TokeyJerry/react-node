
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import NoLinkedStateMixin from './component/NoLinkedStateMixin';
import UseLinkedStateMixin from './component/UseLinkedStateMixin';

class Main extends Component {
  render () {
    return this.props.children;
  }
}

ReactDOM.render(
  <Main >
    <h1>Two-Way binding</h1>
    <NoLinkedStateMixin />
    <UseLinkedStateMixin />
  </Main>,
  document.getElementById('root')
);

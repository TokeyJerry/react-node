
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';

import Counter from './components/Counter';
import counter from '../../store/reducers/react-page';

const store = createStore(counter); // 创建 store ，传入 reducer
const rootDom = document.getElementById('root');

const render = () => ReactDOM.render(
    <Counter
        value={store.getState()}
        onIncrement={() => store.dispatch({ type: 'INCREMENT'})}
        onDecrement={() => store.dispatch({ type: 'DECREMENT'})}
    />,
    rootDom
);

render();

store.subscribe(render);
